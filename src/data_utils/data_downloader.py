from cryptofeed.defines import CANDLES, BID, ASK, BLOCKCHAIN, COINBASE, FUNDING, GEMINI, L2_BOOK, L3_BOOK, OPEN_INTEREST, TICKER, TRADES, FUTURES_INDEX, BOOK_DELTA
from cryptofeed.backends.mongo import BookDeltaMongo, BookMongo, TradeMongo, TickerMongo, MongoCallback, FundingMongo, MarketInfoMongo, CandlesMongo
from cryptofeed.callback import BookCallback, TradeCallback, FundingCallback, CandleCallback
from cryptofeed.exchanges import (Binance, Bitmex, BinanceFutures, BinanceDelivery)
from cryptofeed.backends.arctic import FundingArctic, TickerArctic, TradeArctic
from cryptofeed.raw_data_collection import AsyncFileCallback
from cryptofeed import FeedHandler
from datetime import datetime
import os
#
mongodb_uri = os.environ['MONGODB_URI']

async def ticker(feed, symbol, bid, ask, timestamp, receipt_timestamp):
    print(f'[TICKER] Timestamp: {timestamp} Feed: {feed} Symbol: {symbol} Bid: {bid} Ask: {ask}')

async def trade(feed, symbol, order_id, timestamp, side, amount, price, receipt_timestamp):
    print(f"[TRADE] Timestamp: {timestamp} Cryptofeed Receipt: {receipt_timestamp} Feed: {feed} Symbol: {symbol} ID: {order_id} Side: {side} Amount: {amount} Price: {price}")
    # print(f'Price is {price}')

async def volume(**kwargs):
    print(f"Volume: {kwargs}")


async def futures_index(**kwargs):
    print(f"FuturesIndex: {kwargs}")


async def candle_callback(feed, symbol, start, stop, interval, trades, open_price, close_price, high_price, low_price, volume, closed, timestamp, receipt_timestamp):
    print(f"Candle: {timestamp} {receipt_timestamp} Feed: {feed} Symbol: {symbol} Start: {start} Stop: {stop} Interval: {interval} Trades: {trades} Open: {open_price} Close: {close_price} High: {high_price} Low: {low_price} Volume: {volume} Candle Closed? {closed}")

async def funding(**kwargs):
    print(f"Funding update for {kwargs['feed']}")
    print(kwargs)

async def book(feed, symbol, book, timestamp, receipt_timestamp):
    print(f'Timestamp: {timestamp} Cryptofeed Receipt: {receipt_timestamp} Feed: {feed} Symbol: {symbol} Book Bid Size is {len(book[BID])} Ask Size is {len(book[ASK])}')

async def l2_book(feed, symbol, book, timestamp, receipt_timestamp):
    """similar to the book() function"""
    print(f'Timestamp: {timestamp} Cryptofeed Receipt: {receipt_timestamp} Feed: {feed} Symbol: {symbol} Book {book}')

def nbbo_update(symbol, bid, bid_size, ask, ask_size, bid_feed, ask_feed):
    print(f'[NBBO] Pair: {symbol} Bid Price: {bid} Bid Size: {bid_size:.6f} Bid Feed: {bid_feed} Ask Price: {ask} Ask Size: {ask_size:.6f} Ask Feed: {ask_feed}')


def main():
    config = {'log': {'filename': 'issues.log', 'level': 'INFO'}}
    # spawn a feed handler instance
    f = FeedHandler()
    # NOTE: symbols and channels are defined in the Feed class
    # Binance class supports these 'channels': ['l2_book', 'ticker', 'trades', 'candles'].
    f.add_feed(Binance(symbols=['DOGE-USDT'], channels=[CANDLES, L2_BOOK, TICKER], callbacks={CANDLES: CandlesMongo('binance', uri=mongodb_uri), L2_BOOK: BookMongo('binance', uri=mongodb_uri), 
        TICKER: TickerMongo('binance', uri=mongodb_uri)}, candle_closed_only=True))
    # BinanceDelivery class supports these 'channels': ['funding', 'liquidations', 'l2_book', 'open_interest', 'ticker', 'trades']
    f.add_feed(BinanceDelivery(symbols=['DOGE-USD_PERP'], channels=[CANDLES, FUNDING, TICKER, L2_BOOK], callbacks={CANDLES: CandlesMongo('binance_delivery', uri=mongodb_uri), FUNDING: FundingMongo('binance_delivery', uri=mongodb_uri), TICKER: TickerMongo('binance_delivery', uri=mongodb_uri), L2_BOOK: BookMongo('binance_delivery', uri=mongodb_uri)}, candle_closed_only=True))
    # BinanceFutures class supports these 'channels': ['funding', 'liquidations', 'l2_book', 'open_interest', 'ticker', 'trades', 'candles']
    f.add_feed(BinanceFutures(symbols=['DOGE-USDT'], channels=[CANDLES, FUNDING, TICKER, L2_BOOK], callbacks={CANDLES: CandlesMongo('binance_futures', uri=mongodb_uri), FUNDING: FundingMongo('binance_futures', uri=mongodb_uri), TICKER: TickerMongo('binance_futures', uri=mongodb_uri), L2_BOOK: BookMongo('binance_futures', uri=mongodb_uri)}, candle_closed_only=True))
    f.run()

if __name__ == '__main__':
    main()

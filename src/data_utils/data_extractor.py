from sklearn.preprocessing import MinMaxScaler
from datetime import datetime, timedelta
from bson.objectid import ObjectId
from pymongo import MongoClient
from pytz import timezone
import pandas as pd
import numpy as np
import argparse
import bson

class Extractor:
    """
    A class used to represent a data extractor

    Attributes
    ----------

    Methods
    -------
    """
    def __init__(self, market='binance_spot', from_date=None,
            to_date=None, db_host=None, db_port=None, 
            user_name=None, password=None, cluster_uri=None, time_format="%Y-%m-%d %H:%M:%S", 
            query_field='receipt_timestamp', data={}):
        """
        Parameters
        ----------
        market : str
            a market that we want to extract data from the Mongodb database
        from_date : object
            the start of the query interval
        to_date : object
            the end of the query interval
        db_host : str
            the address of the database
        db_port: int
            the port number that we want to connect to the Mongodb database
        time_format: str
            the format that we want to represent the time inputs
        query_field: str
            the field that we want to apply our query rule
        """
        self.market = market

        #self.from_date = datetime.timestamp(datetime.strptime(from_date,
        #    time_format))
        #self.to_date = datetime.timestamp(datetime.strptime(to_date,
        #    time_format))
        from_date_cvt = timezone('UTC').localize(from_date)
        to_date_cvt = timezone('UTC').localize(to_date)
        self.from_date_name = from_date_cvt.strftime('%Y_%m_%d')
        self.to_date_name = to_date_cvt.strftime('%Y_%m_%d')
        self.from_date = datetime.timestamp(from_date_cvt)
        self.to_date = datetime.timestamp(to_date_cvt)
        self.db_host = db_host
        self.db_port = db_port
        self.user_name = user_name
        self.password = password
        self.cluster_uri = cluster_uri
        self.query_field = query_field
        self.data = data


    def connect_and_query(self, channel):
        """Connects to the Mongodb database and applies the query rule
        Parameters
        ---------
        channel
            channel/collection that we want to get documents out
        Returns
        -------
        documents
            queried documents
        """
        if self.user_name and self.password:
            self.client = MongoClient(self.cluster_uri.format(self.user_name, self.password))
        else:
            self.client = MongoClient(self.db_host, self.db_port)
        collections = self.client[self.market]
        channel_collection = collections[channel]
        if self.query_field == 'receipt_timestamp':
            my_query_rule = {self.query_field: {'$lt': self.to_date,
                '$gte': self.from_date}}
            documents = channel_collection.find(my_query_rule)

        return documents

    def extract_from_candles(self):
        """Extracts info from the Mongodb database to create wicks and volume
        Parameters
        ----------
        None

        Returns
        -------
        list
            a list that contains time steps which are used as time references
            to extract information from the order book
        """
        documents = self.connect_and_query(channel='candles')
        receipt_times = []
        for document in documents:
            receipt_time = pd.to_datetime(document['receipt_timestamp'], unit='s').round('1T')
            #receipt_time = pd.to_datetime(document['receipt_timestamp']).dt.floor('Min')
            receipt_times.append(receipt_time)
            datetime_list = self.data.get('Date', [])
            datetime_list.append(receipt_time)
            self.data['Date'] = datetime_list
            # add low prices to the dictionary
            low_price_list = self.data.get('Low', [])
            low_price_list.append(document['low_price'])
            self.data['Low'] = low_price_list
            # add high prices to the dictionary
            high_price_list = self.data.get('High', [])
            high_price_list.append(document['high_price'])
            self.data['High'] = high_price_list
            # add open prices to the dictionary
            open_price_list = self.data.get('Open', [])
            open_price_list.append(document['open_price'])
            self.data['Open'] = open_price_list
            # add closed prices to the dictionary
            close_price_list = self.data.get('Close', [])
            close_price_list.append(document['close_price'])
            self.data['Close'] = close_price_list
            # add volume to the dictionary
            volume_list = self.data.get('Volume', [])
            volume_list.append(document['volume'])
            self.data['Volume'] = volume_list

        return receipt_times

    def extract_from_l2book(self):
        """Extracts info from the oderbook to calculate thicknesses of markets
        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        documents = self.connect_and_query(channel='book')
        for document in documents:
            ask_prices = list(map(float, list(bson.BSON(document['ask']).decode().keys())))
            ask_sizes = list(map(float, list(bson.BSON(document['ask']).decode().values())))
            ask_products = [p * s for p, s in zip(ask_prices, ask_sizes)]
            ask_weighted_avg = sum(ask_products)/sum(ask_sizes)
            best_ask = ask_prices[0]
            ask_thickness = abs(best_ask/(ask_weighted_avg - best_ask))

            bid_prices = list(map(float, list(bson.BSON(document['bid']).decode().keys())))
            bid_sizes = list(map(float, list(bson.BSON(document['bid']).decode().values())))
            bid_products = [p * s for p, s in zip(bid_prices, bid_sizes)]
            bid_weighted_avg = sum(bid_products)/sum(bid_sizes)
            best_bid = bid_prices[0]
            bid_thickness = abs(best_bid/(bid_weighted_avg - best_bid))

            ask_thickness_list = self.data.get('AThickness', [])
            ask_thickness_list.append(ask_thickness)
            self.data['AThickness'] = ask_thickness_list
            bid_thickness_list = self.data.get('BThickness', [])
            bid_thickness_list.append(bid_thickness)
            self.data['BThickness'] = bid_thickness_list

    def save_to_csv(self, output_path):
        """Save data extracted from Mongodb to a csv file, ready for further processing
        Parameters
        ---------
        None

        Returns
        -------
        None
        """
        df = pd.DataFrame.from_dict(self.data, orient='index').T
        #df = df.set_index('Date')
        #df = df.replace(np.nan, 0)
        df.to_csv(output_path + f'{self.market}_{self.from_date_name}_{self.to_date_name}.csv')

    def create_df(self):
        """Create a dataframe for testing purposes
        Parameters
        ----------
        None

        Returns
        -------
        None
        """
        df = pd.DataFrame.from_dict(self.data, orient='index').T
        #df = df.set_index('Date')
        #df = df.replace(np.nan, 0)
        return df

class BNFutures(Extractor):
    """
    A class that inherits the Extractor class, where the candle channel and orderbook channel can be reused

    Attributes
    ----------

    Methods
    -------

    """
    def extract_from_funding(self, time_references):
        timeref_length = len(time_references)
        documents = self.connect_and_query(channel='funding')
        counter = 0
        for document in documents:
            receipt_time = pd.to_datetime(document['receipt_timestamp'], unit='s').round('1T')
            #receipt_time = pd.to_datetime(document['receipt_timestamp']).dt.floor('Min')
            if receipt_time in time_references:
                counter += 1
                if counter > timeref_length:
                    break
                funding_list = self.data.get('MarkPrice', [])
                funding_list.append(document['mark_price'])
                self.data['MarkPrice'] = funding_list

                interest_rate_list = self.data.get('FundingRate', [])
                interest_rate_list.append(document['rate'])
                self.data['FundingRate'] = interest_rate_list
            else:
                continue

class BNDelivery(Extractor):
    """
    A class that inherits the Extractor class, where the candle channel and orderbook channel can be reused

    Attributes
    ----------

    Methods
    -------

    """
    def extract_from_funding(self, time_references):
        timeref_length = len(time_references)
        documents = self.connect_and_query(channel='funding')
        counter = 0
        for document in documents:
            receipt_time = pd.to_datetime(document['receipt_timestamp'], unit='s').round('1T')
            #receipt_time = pd.to_datetime(document['receipt_timestamp']).dt.floor('Min')
            if receipt_time in time_references:
                counter += 1
                if counter > timeref_length:
                    break
                funding_list = self.data.get('MarkPrice', [])
                funding_list.append(document['mark_price'])
                self.data['MarkPrice'] = funding_list

                interest_rate_list = self.data.get('FundingRate', [])
                interest_rate_list.append(document['rate'])
                self.data['FundingRate'] = interest_rate_list
            else:
                continue

if __name__ == '__main__':
    # construct argument parser
    ap = argparse.ArgumentParser()
    ap.add_argument('-m', '--market', required=True, help='choose the market to extract the raw data')
    ap.add_argument('-fd', '--from-date', type=lambda s: datetime.strptime(s, "%Y-%m-%d"), required=True, help='choose the starting date, e.g. 2021-06-11 (%Y-%m-%d) UTC time')
    ap.add_argument('-td', '--to-date', type=lambda s: datetime.strptime(s, "%Y-%m-%d"), required=True, help='choose the ending date, e.g. 2021-06-11 (%Y-%m-%d) UTC time')
    ap.add_argument('-o', '--output', required=True, help='path to save the extracted data')
    args = vars(ap.parse_args())

    cluster0 = "mongodb+srv://{}:{}@cluster0.cv0wr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
    cluster1 = "mongodb+srv://{}:{}@cluster1.cv0wr.mongodb.net/dogedb?retryWrites=true&w=majority"

    if args['market'] == 'binance_spot':
        bn = Extractor(user_name='KL', password='ubuntu20211st', from_date=args['from_date'], to_date=args['to_date'], cluster_uri=cluster1)
        bn.extract_from_candles()
        bn.extract_from_l2book()
        bn.save_to_csv(args['output'])
    elif args['market'] == 'binance_futures':       
        bn_futures = BNFutures(market='binance_futures', user_name='KL', password='ubuntu20211st', from_date=args['from_date'], to_date=args['to_date'], cluster_uri=cluster1)
        bnft_time_refs = bn_futures.extract_from_candles()
        bn_futures.extract_from_l2book()
        bn_futures.extract_from_funding(bnft_time_refs)
        bn_futures.save_to_csv(args['output'])
    else:
        # spawn another instance
        bn_delivery = BNDelivery(market='binance_delivery', user_name='KL', password='ubuntu20211st', from_date=args['from_date'], to_date=args['to_date'], cluster_uri=cluster1)
        bndlv_time_refs = bn_delivery.extract_from_candles()
        bn_delivery.extract_from_l2book()
        bn_delivery.extract_from_funding(bndlv_time_refs)
        bn_delivery.save_to_csv(args['output'])

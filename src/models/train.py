from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import plot_confusion_matrix
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import json
import os

# read the data in
df_train = pd.read_csv('../../data/processed/binance_delivery_futures_train.csv')
df_test = pd.read_csv('../../data/processed/binance_delivery_futures_test.csv')
df_train.drop('closing_time', inplace=True, axis=1)
df_test.drop('closing_time', inplace=True, axis=1)

# create feature vectors
X_train = df_train.iloc[:, :-2].values
y_train = df_train.iloc[:, -1].values

X_test = df_test.iloc[:, :-2].values
y_test = df_test.iloc[:, -1].values

classifier = RandomForestClassifier(n_estimators=1000, max_depth=16, criterion='entropy', min_samples_split=5)
classifier.fit(X_train, y_train)

acc = classifier.score(X_test, y_test)
with open('metrics.txt', 'w') as f_obj:
    f_obj.write('Accuracy: ' + str(acc) + '\n')

disp = plot_confusion_matrix(classifier, X_test, y_test, normalize='true', cmap=plt.cm.Blues)
plt.savefig('confusion_matrix.png')

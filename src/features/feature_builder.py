# import necessary packages
from ta import add_all_ta_features
import matplotlib.pyplot as plt
from datetime import datetime
import seaborn as sns
import pandas as pd
import numpy as np
import itertools
import yaml
import glob
import sys
import os

class FeatureBuilder:

    def __init__(self, input_path_1, input_path_2, interval=5):
        self.input_path_1 = input_path_1
        self.input_path_2 = input_path_2
        self.interval = interval

    def load_dataframes(self):
        df1 = pd.read_csv(self.input_path_1)
        df2 = pd.read_csv(self.input_path_2)
        return df1, df2

    def resample_ohlcv(self, df, period):
        """The function supports aggregating data points together in case we need it."""
        # set date as index
        # needed for the function to run
        df = df.set_index(['date'])

        # aggregation function
        if 'FundingRate' in df:
            ohlc_dict = {
                'Open':'first',
                'High':'max',
                'Low':'min',
                'Close': 'last',
                'Volume': 'sum',
                'FundingRate': 'last'
            }

        else:
            ohlc_dict = {
                'Open':'first',
                'High':'max',
                'Low':'min',
                'Close': 'last',
                'Volume': 'sum',
            }
        # resample
        df = df.resample(period, closed='left', label='left').agg(ohlc_dict)

        return df

    def fill_nan(self, df):
        """The function handles *na* values in data frames"""
        # Forward fill close column
        df['Close'] = df['Close'].ffill()

        # Backward fill the open, high, low rows with the close value
        df = df.bfill(axis=1)

        return df

    def engineer_features(self, df, period='T'):
        """The function adds technical indicators to the data frames"""
        # convert unix closing_time to datetime
        df['date'] = pd.to_datetime(df['Date'])

        # time resampling to fill gaps in data
        df = self.resample_ohlcv(df, period)
        df = df.reset_index()
        closing_time = df.date.values
        df.drop(columns='date', inplace=True)
        df['nan_ohlcv'] = df['Close'].apply(lambda x: 1 if pd.isnull(x) else 0)
        df = self.fill_nan(df)
        df = add_all_ta_features(df, 'Open', 'High', 'Low', 'Close','Volume', fillna=True)
        # add closing time column
        df['closing_time'] = closing_time
        return df

    def get_higher_closing_price(self, df):
        # exchange 1 has higher closing price
        if (df['Close_exchange_1'] - df['Close_exchange_2']) > 0:
            return 1

        # exchange 2 has higher closing price
        elif (df['Close_exchange_1'] - df['Close_exchange_2']) < 0:
            return 2

        # closing prices are equivalent
        else:
            return 0

    def get_pct_higher(self, df):
        # if exchange 1 has a higher closing price than exchange 2
        if df['higher_closing_price'] == 1:

            # % difference
            return (1 - (df['Close_exchange_2'] / df['Close_exchange_1'])) * 100
            #return ((df['Close_exchange_1'] / df['Close_exchange_2'])-1)*100

        # if exchange 2 has a higher closing price than exchange 1
        elif df['higher_closing_price'] == 2:

            # % difference
            return (1 - (df['Close_exchange_1'] / df['Close_exchange_2'])) * 100
            #return ((df['Close_exchange_2'] / df['Close_exchange_1'])-1)*100

        # if closing prices are equivalent
        else:
            return 0


    def get_arb_opportunity(self, df):
        if df['pct_higher'] < .05:
            return 0 # no arbitrage

        # if exchange 1 closing price is more than 0.05% higher
        # than the exchange 2 closing price
        elif df['higher_closing_price'] == 1:
            return -1 # arbitrage from exchange 2 to exchange 1

        # if exchange 2 closing price is more than 0.15% higher
        # than the exchange 1 closing price
        elif df['higher_closing_price'] == 2:
            return 1 # arbitrage from exchange 1 to exchange 2


    def get_window_length(self, df):
        """After opening a position, we need to be assured that the opportunity lasts long enough.
        """
        # convert the arbitrage opportunity column to a list
        target_list = df['arbitrage_opportunity'].to_list()
        # initialize a window length
        window_length = 1
        # list for window lengths
        window_lengths = []
        for i in range(len(target_list)):
            if target_list[i] == target_list[i-1]:
                window_length += 1
                window_lengths.append(window_length)
            else:
                window_length = 1
                window_lengths.append(window_length)
        df['window_length'] = window_lengths

        return df

    def merge_dfs(self, df1, df2):
        df = pd.merge(df1, df2, on='closing_time', suffixes=('_exchange_1', '_exchange_2'))
        df['closing_time'] = pd.to_datetime(df['closing_time'])
        df['year'] = df['closing_time'].dt.year
        df['month'] = df['closing_time'].dt.month
        df['day'] = df['closing_time'].dt.day
        df['hour'] = df['closing_time'].dt.hour
        df['minute'] = df['closing_time'].dt.minute
        df['second'] = df['closing_time'].dt.second
        print('before get higher closing price', df.shape)
        df['higher_closing_price'] = df.apply(self.get_higher_closing_price, axis=1)
        # get pct_higher feature to create arbitrage_opportunity feature
        df['pct_higher'] = df.apply(self.get_pct_higher, axis=1)

        # create arbitrage_opportunity feature
        df['arbitrage_opportunity'] = df.apply(self.get_arb_opportunity, axis=1)
        # create window_length feature
        df = self.get_window_length(df)

        return df

    def get_target_value(self, df):
        if df['window_length_shift'] >= self.interval:
            if df['arbitrage_opportunity_shift'] == 1:
                return 1
            elif df['arbitrage_opportunity_shift'] == -1:
                return -1
            elif df['arbitrage_opportunity_shift'] == 0:
                return 0
        else:
            return 0

    def get_target(self, df):

        interval = self.interval
        rows_to_shift = int(-1*(interval/1)) # -5
        df['arbitrage_opportunity_shift'] = df['arbitrage_opportunity'].shift(
            rows_to_shift - 4) # move 9 rows up
        df['window_length_shift'] = df['window_length'].shift(rows_to_shift - 4)
        df['target'] = df['arbitrage_opportunity_shift']
        df = df[:rows_to_shift] # -5
        df['target'] = df.apply(self.get_target_value, axis=1)
        df = df[:rows_to_shift - 4]
        return df

if __name__ == '__main__':
    params = yaml.safe_load(open('params.yaml'))['featurize']
    def generate_output_paths(first_dir, second_dir):
        first_exchange_name = first_dir.split(os.path.sep)[-1].split('.')[0].split('_')[0]
        second_exchange_name = second_dir.split(os.path.sep)[-1].split('.')[0].split('_')[0]
        first_market_name = first_dir.split(os.path.sep)[-1].split('.')[0].split('_')[1]
        second_market_name = second_dir.split(os.path.sep)[-1].split('.')[0].split('_')[1]
        if first_exchange_name == second_exchange_name:
            # choose either of them
            output_path_train = os.path.join('data', 'processed', f'{first_exchange_name}_{first_market_name}_{second_market_name}_train.csv')
            output_path_test = os.path.join('data', 'processed', f'{first_exchange_name}_{first_market_name}_{second_market_name}_test.csv')
            return output_path_train, output_path_test
        else:
            print('[INFO] It will behandled in the future versions.')

    raw_data_path = sys.argv[1]
    csv_filepaths = glob.glob(os.path.join(raw_data_path, '*.csv'))
    combos = list(itertools.combinations(csv_filepaths, 2))
    for combo in combos:
        first_input_path = combo[0]
        second_input_path = combo[1]
        output_train, output_test = generate_output_paths(first_input_path, second_input_path)
        feat_builder = FeatureBuilder(first_input_path, second_input_path, 5)
        df1, df2 = feat_builder.load_dataframes()
        df1_ta = feat_builder.engineer_features(df1)
        df2_ta = feat_builder.engineer_features(df2)
        df = feat_builder.merge_dfs(df1_ta, df2_ta)
        df = feat_builder.get_target(df)
        # get the number of test days from the params.yaml file
        test_days = params['test_days'] * 1439 # with one min interval, 1439 data points will bebe ingested to the mongodb
        train_df = df[:-test_days]
        test_df = df[-test_days:]
        os.makedirs(os.path.join('data', 'processed'), exist_ok=True)
        train_df.to_csv(output_train)
        test_df.to_csv(output_test)
